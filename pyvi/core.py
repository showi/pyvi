import logging
logging.basicConfig()
log = logging.getLogger('Pyvi:Core')
log.setLevel(logging.INFO)
from threading import Thread
from Queue import Queue
from pyvi.resolver import Resolver


class Worker(Thread):
    """Thread executing tasks from a given tasks queue"""
    def __init__(self, tasks):
        Thread.__init__(self)
        self.tasks = tasks
        self.daemon = True
        self.start()

    def run(self):
        while True:
            func, args, kargs = self.tasks.get()
            try:
                func(*args, **kargs)
            except Exception, e:
                log.error(e)
            self.tasks.task_done()


class ThreadPool:
    """Pool of threads consuming tasks from a queue"""
    def __init__(self, num_threads):
        self.tasks = Queue(num_threads)
        for _ in range(num_threads):
            Worker(self.tasks)

    def add_task(self, func, *args, **kargs):
        """Add a task to the queue"""
        self.tasks.put((func, args, kargs))

    def wait_completion(self):
        """Wait for completion of all the tasks in the queue"""
        self.tasks.join()


class MediaData(object):
    def __init__(self, url):
        self.request_url = url
        self.url = None
        self.request = None

    def resolve(self):
        self.url = None
        self.request = None
        resolver = Resolver(self.request_url)
        r = resolver.resolve()
        if r.status_code != 200:
            log.error('Cannot resolve url %s', self.request_url)
            return False
        self.request = r
        self.url = r.url
        return True

    def dump(self):
        log.info('Find plugin to dump information!')

    def __str__(self):
        s = '<MediaData request_url="%s", url="%s">' % (self.request_url,
                                                    self.url)
        return s


def job_dump_url(url):
    log.info('Dump url %s', url)
    d = MediaData(url)
    if not d.resolve():
        return False
    log.info(d)
    return True


class Core(object):

    def __init__(self):
        self.pool = ThreadPool(10)

    def dump(self, url):
        self.pool.add_task(job_dump_url, url)

    def wait_completion(self):
        self.pool.wait_completion()

core = Core()
