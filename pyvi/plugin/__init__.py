
plugins = {}


class Plugin(object):

    def __init__(self):
        if not hasattr(self, 'name'):
            raise ValueError('Plugin must have <<name>> attribute')

    @classmethod
    def decorator(self, cls):
        print "Decorate: %s" % cls
        name = cls.name
        if name in plugins:
            raise RuntimeError('Plugin with same name already registered')
        plugins[name] = cls
        return cls

    def can_process(self):
        raise NotImplementedError('can_process method must be implemented')


@Plugin.decorator
class TestPlugin(Plugin):
    name = 'TestPlugin'

    def can_process(self):
        print "Test can process"


if __name__ == '__main__':
    p = plugins['TestPlugin']()
    p.can_process()
