import logging
logging.basicConfig()
log = logging.getLogger('Pyvi:Resolver')
log.setLevel(logging.INFO)
import requests


class Resolver(object):

    def __init__(self, url):
        self.url = url

    def resolve(self, url=None):
        if url is not None:
            self.url = url
        url = self.url
        r = requests.get(url)
        log.info(r.history)
        log.info(r.status_code)
        for rh in r.history:
            log.info('History: %s' % rh.headers['location'])
#         log.info('Headers\n%s', r.headers)
        log.info(r.url)
        return r
